Name:       migmix-1m
Version:    20150712
Release:    1%{?dist}
Summary:    Japanese TrueType Font

Group:      User Interface/X
License:    IPA Font License Agreement v1.0
URL:        http://mix-mplus-ipa.sourceforge.jp/migmix/
Source0:    %{name}-%{version}.zip

BuildArch:  noarch

#SRC_URL http://iij.dl.sourceforge.jp/mix-mplus-ipa/63544/migmix-1m-20150712.zip

#BuildRequires:
#Requires:

%description

%prep

%setup -q 

%build
%{nil}

%global fontdir %{_datadir}/fonts

%install
%{__install} -dm 755 %{buildroot}%{_defaultdocdir}/%{name}
%{__install} -dm 755 %{buildroot}%{fontdir}/%{name}

%{__install} -m 644 *.ttf            %{buildroot}%{fontdir}/%{name}/
%{__install} -m 644 *README.txt      %{buildroot}%{_defaultdocdir}/%{name}/
%{__install} -m 644 ipag00303/*.txt  %{buildroot}%{_defaultdocdir}/%{name}/

%post
/usr/bin/fc-cache -f %{buildroot}%{fontdir}/%{name}

%files
%doc
%{fontdir}
%{_defaultdocdir}/%{name}


%changelog

